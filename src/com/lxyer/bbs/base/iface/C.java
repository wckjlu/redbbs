package com.lxyer.bbs.base.iface;

/**
 * 创建信息
 * Created by liangxianyou at 2018/6/16 17:43.
 */
public interface C<I extends CI> {
    I createInfo();
}
